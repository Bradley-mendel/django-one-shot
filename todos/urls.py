from django.urls import path
from todos.views import todo_list_list, todo_list_detail, create_todos, edit_todo, delete_todo, create_todo_item

urlpatterns = [
    path("", todo_list_list, name="todo_list_list"),
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("create/", create_todos, name="todo_list_create"),
    path("<int:id>/edit/", edit_todo, name = "todo_list_update"),
    path("<int:id>/delete/", delete_todo, name="todo_list_delete"),
    path("items/create/", create_todo_item, name="todo_item_create"),

]
