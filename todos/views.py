from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, TodoItemForm

def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list": todos,
    }
    return render(request, "todo/list.html", context)

def todo_list_detail(request, id):
    todo_lists = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_detail": todo_lists
    }
    return render(request, "todo/detail.html", context)

def create_todos(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            todos = form.save()
            return redirect("todo_list_detail", id=todos.id)
    else:
        form = TodoForm()
    context = {
            "form": form,
        }
    return render(request, "todo/create.html", context)

def create_todo_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todos = form.save()
            return redirect("todo_list_detail")
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "todo/create_item.html", context)

def edit_todo(request, id):
    todo_lists = TodoList.objects.get(id = id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todo_lists)
        if form.is_valid():
            todo_lists = form.save()
            return redirect("todo_list_detail", id=todo_lists.id)
    else:
        form = TodoForm(instance=todo_lists)
    context = {
            "form": form
        }
    return render(request, "todo/edit.html", context)

def delete_todo(request, id):
    todos_list = TodoList.objects.get(id = id)
    if request.method == "POST":
        todos_list.delete()
        return redirect("todo_list_list")
    return render(request, "todo/delete.html")
